﻿using System;

/*
 * This is the basic console calculator using C sharp.
 * There are four operators: Addition, Subtraction, Multiplication, and Division
 * V1.0 No condition check.
 * V2.0 will add condition check as needed. Allow code reviewer, find something.
 */
namespace BasicCalculator
{
    class Program
    {
        static int num1 , num2 ;
        static int answer = 2;
        static void Main(string[] args)
        {
            //set default numbers
            num1 = 8;
            num2 = 4;

               //Print title and get new line
                Console.WriteLine("Basic Calculator\n");

                //get user input num1
                Console.Write("Enter the first integer: ");
                num1 = Convert.ToInt32(Console.ReadLine());
 
                //get user input num2
                Console.Write("Enter the second integer: ");
                num2 = Convert.ToInt32(Console.ReadLine());

             
            //call Addition and display result on console
            CalculationFunctions.DisplayResult(num1, num2, CalculationFunctions.AddNumbers(num1, num2), "+");

            //call Subtraction method and display result on console
            CalculationFunctions.DisplayResult(num1, num2, CalculationFunctions.SubtractNumbers(num1, num2),"-");

            //call Multiplication and display result on console
            CalculationFunctions.DisplayResult(num1, num2, CalculationFunctions.MultiplyNumbers(num1, num2),"*");

            //call division and display result on console
            CalculationFunctions.DisplayResult(num1, num2, CalculationFunctions.DivideNumbers(num1, num2),"/");

            // exit
            Console.ReadLine();
        }//end main
    }//end class Program
}// end name space BasicCalculator
