﻿using System;
using System.Collections.Generic;
using System.Text;

//version 1.0: No condition check numbers
namespace BasicCalculator
{
    class CalculationFunctions
    {
        //Addition 
        public static int AddNumbers(int a, int b)
        {
            return a + b;
        }
        //SubStraction
        public static int SubtractNumbers(int a, int b)
        {
            return a - b;
        }
        //Division
        public static int DivideNumbers(int a, int b)
        {
            return a / b;
        }
        //Multiplication
        public static int MultiplyNumbers(int a, int b)
        {
            return a * b;
        }
  
        //Displaying result of each opertion on the console
        public static void DisplayResult(int a, int b, int re, string opt)
        {
            Console.WriteLine(a.ToString() + " " + opt + " " + b.ToString() + " = " + re.ToString());

        }
    }
}
